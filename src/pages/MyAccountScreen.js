import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from "react-native";

import {Avatar} from 'react-native-elements';
import firebase from 'firebase';
import { Icon,  Container, Header,
Left, Right, Body } from 'native-base'

class MyAccountScreen extends Component {

  displayName=firebase.auth().currentUser.email

  handleAccount=()=>{
    this.props.navigation.navigate('MyAccountScreen')
  }

  handleSettings=()=>{
    this.props.navigation.navigate('SettingsScreen')
  }
    
  render() {
    return (
      <Container>
        <Header style={styles.header}>
        <Left>
        <Icon name='person' onPress={this.handleAccount} style={{color:'white'}} />
        </Left>
        <Body>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HomeScreen')}}>
          <Image style={{width:65, height:65, flex:0, alignItems:'center'}} source={require('../img/logo.png')}/>
          </TouchableOpacity>
        </Body>
        <Right>
        <Icon name='settings' onPress={this.handleSettings} style={{color:'white'}}/>
        </Right>  
        </Header>
        <View style={styles.nameTab}>
        <Avatar 
          large
          rounded
          source={require('../img/logo.png')}
          activeOpacity={0.7}
          
         />
         <Text style={{paddingHorizontal:10}}>Welcome {this.displayName}</Text>
         </View>
         <View
         style={{
           borderBottomColor: 'rgba(211,211,211,0.5)',
           borderBottomWidth: 1,
          }}
/>
        <View style={styles.container}>
        <Body style={styles.bodyStyle}>

        </Body>
        </View>
        </Container>
    );
  }
}
export default MyAccountScreen;

const styles=StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  header:{
    backgroundColor:'#08a529'
  },
  bodyStyle:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  nameTab:{
    flex:0,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    paddingVertical:10,
    paddingHorizontal:10
  }
})