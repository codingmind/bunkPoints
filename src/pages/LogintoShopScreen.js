import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,

} from "react-native";

import { Icon, Button, Container, Header, Form,Item, Input, Left, Label } from 'native-base';
import firebase from 'firebase';



class LogintoShopScreen extends Component {

  constructor(props){
    super(props)
  
    this.state = ({
      shop: '',
      admin: '' 
    });
  } 


      componentWillMount(){
       /*config.js goes here*/

  

  }
     
      ShopSignUp=(shop,admin)=>{
      var user=firebase.auth().currentUser;
        firebase.database().ref(user.uid).set(
          {
            shopName: this.state.shop,
            adminName: this.state.admin
          }
        ).then(()=>{this.props.navigation.navigate('ShopScreen')})
      }
  render() {
    return (
      <Container>

           <Header style={styles.header}>
        <Left>
        <Icon name="ios-menu" onPress={() =>
        this.props.navigation.openDrawer()} />
        </Left>
          </Header>
        <View style = {styles.container}>

      <StatusBar
              backgroundColor='#2286c3'
              barStyle="dark-content"
      />

     <Image style={{width:100, height:100}}
                source={require('../img/logo.png')}/>
                <Text style={styles.logoText}>BunkPoints</Text>

       <Form>

        <Item floatingLabel style={styles.inputText}>
         <Label>Shop Name</Label>
         <Input
           autoCorrect={false}
           autoCapitalize="none"
           onChangeText={(shop)=>this.setState({shop})}
           />
           </Item>
        <Item floatingLabel style={styles.inputText}>
         <Label>Admin Name</Label>
         <Input
           autoCorrect={false}
           autoCapitalize="none"
           onChangeText={(admin) => this.setState({admin})}/>
          </Item>
              <Button style={styles.clickButton}
                full
                rounded
                success
                onPress={() => this.ShopSignUp(this.state.shop, this.state.admin)}
               ><Text style={{fontWeight: 'bold', color:'white'}}>SignUp</Text>
              </Button>
       </Form>
      </View>
      </Container>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#63b4f4',
    justifyContent: 'center',
    padding: 10,
    justifyContent:'center',
    alignItems: 'center'
  },
  logoText:{
    marginVertical: 15,
    fontSize:22,
    color: 'white',
    justifyContent:'flex-end',
    alignItems: 'center'
},
   header:{
      backgroundColor:'#2286c3',
       height: 25
   },
inputText:{
  width:250,
  alignItems:'center',
  marginVertical: 10,
},
clickButton:{
  paddingVertical:15,
  marginVertical:15,
}
});
export default LogintoShopScreen;