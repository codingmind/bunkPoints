import React, {Component} from 'react';
import { StyleSheet, TextInput, View, StatusBar, Image, Text, Alert } from 'react-native';
import * as firebase from 'firebase';
import {firebaseConfig} from "../../config";

firebase.initializeApp(firebaseConfig);
 

import { Container, Content,  Form, Input, Item, Button, Label, } from 'native-base'

loading='true'


export default class App extends Component {

constructor(props){
  super(props)

  this.state = ({
    email: '',
    password: '' 
  });
}  

 signUpUser = (email, password) =>{

    if(this.state.password.length<6){
      Alert.alert("Please enter atleast 6 characters")} 
      firebase.auth().createUserWithEmailAndPassword(email,password).then(()=>{this.props.navigation.navigate('MyApp')}).catch(error=>{switch(error.code)
        {case 'auth/email-already-in-use':Alert.alert("Email already registered")
                                          break
         case 'auth/invalid-email':Alert.alert("Invalid email address")
                                   break }})    
      var user=firebase.auth().currentUser;
      

      
    }

     

 loginUser = (email, password) =>{

 firebase.auth().signInWithEmailAndPassword(email,password)
    .then(()=>
    {
      this.props.navigation.navigate('MyApp')
    }).catch(error=>{switch(error.code)
      {case 'auth/user-not-found':Alert.alert("User not found. New user? SignUp")
                                        break
       case 'auth/wrong-password':Alert.alert("Wrong Password")
                                 break }})
}

  render() {
    return (

      <View style = {styles.container}>

      <StatusBar
              backgroundColor='#08a529'
              barStyle="dark-content"
      />
     
     <Image style={{width:100, height:100}}
                source={require('../img/logo.png')}/>
                <Text style={styles.logoText}>BunkPoints</Text>
        
       <Form>
        
        <Item floatingLabel style={styles.inputText}>
         <Label>Email</Label>
         <Input
           autoCorrect={false}
           autoCapitalize="none"
           onChangeText={(email)=>this.setState({email})}
           />       
           </Item>
        <Item floatingLabel style={styles.inputText}>
         <Label>Password</Label>
         <Input
           secureTextEntry={true}
           autoCorrect={false}
           autoCapitalize="none"
           onChangeText={(password) => this.setState({password})}           />
          </Item>
        
              <Button style={styles.clickButton}
                full
                rounded
                success
                onPress={() => this.loginUser(this.state.email, this.state.password)}
               >
                         <Text style={{fontWeight: 'bold', color:'white'}}>Log In</Text>
              </Button>

              <Button style={styles.clickButton}
                full
                rounded
                primary
                onPress={() => this.signUpUser(this.state.email, this.state.password)}
               >
                         <Text style={{fontWeight: 'bold', color: 'white'}}>Sign Up</Text>
              </Button>
       </Form>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    padding: 10,
    justifyContent:'center',
    alignItems: 'center'
  },
  logoText:{
    marginVertical: 15,
    fontSize:22,
    color: 'white',
    justifyContent:'flex-end',
    alignItems: 'center'
},
inputText:{
  width:250,
  alignItems:'center',
  marginVertical: 10,
},
clickButton:{
  paddingVertical:15,
  marginVertical:15,
}
});
