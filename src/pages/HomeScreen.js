import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
} from "react-native";

import { Icon, Button, Container, Header, Content,
Left, Right, Body} from 'native-base'
import firebase from 'firebase';
import QRCode from 'react-native-qrcode-svg';
import Swiper from 'react-native-swiper';
import Modal from 'react-native-modal';


class HomeScreen extends Component {

handleLogin=()=>{this.props.navigation.navigate('LoadingShop')}

getDataURL(){
  this.svg.toDataURL(this.callback);
  
}

handleSettings=()=>{
  this.props.navigation.navigate('SettingsScreen')
}

handleAccount=()=>{
  this.props.navigation.navigate('MyAccountScreen')
}
  render() {
    return (
      <Container>
        <Header style={styles.header}>
        <Left>
        <Icon name='person' onPress={this.handleAccount} style={{color:'white'}} />
        </Left>
        <Body>
          <Image style={{width:65, height:65, flex:0, alignItems:'center'}} source={require('../img/logo.png')} onPress={this.props.navigation.navigate('HomeScreen')}/>
        </Body>
        <Right>
        <Icon name='settings' onPress={this.handleSettings} style={{color:'white'}}/>
        </Right>  
        </Header>
        <Body style={styles.bodyStyle}>
        
        <View style={styles.slide}>
        <QRCode
          value={firebase.auth().currentUser.uid}
          getRef={(c)=>(this.svg=c)}
          size={150}  
        /> 
        </View>
             
        <Content style={styles.container}>
        <Button style={styles.clickButton}
                full
                rounded                
                onPress={() => this.handleLogin()}>
        <Text style={{fontWeight: 'bold', color:'white'}}>Switch to Shop</Text>
        </Button>  
        </Content>
        </Body>  
        </Container>

    );
  }
}

export default HomeScreen;

const styles=StyleSheet.create({
container:{
  flex:1,},
wrapper:{},
slide:{
  flex:1,
  justifyContent:'center',
  alignItems:'center'
},
header:{
  backgroundColor:'#08a529'
},  
bodyStyle:{
  flex:1,
  alignItems:'center',
  justifyContent:'center',
},
clickButton:{
  paddingVertical:15,
  marginVertical:15,
  backgroundColor:'#08a529',
  width:250,
},
modalStyle:{
  
  backgroundColor:'white',
  height:500,
}
})
