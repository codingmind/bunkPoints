import React, { Component, PropTypes } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    Alert,
    TouchableOpacity,
  } from "react-native"; 
import {Button, Body, Header, Container, Left, Right, Icon} from 'native-base';
import {Camera, Permissions, Notifications} from 'expo';
import Prompt from 'rn-prompt';
import firebase from 'firebase';

export default class ShopScreen extends Component {
    state={
        hasCameraPermission:null,
        promptVisible:false,
        message:'',
    };
    askPermission=async()=>{
        const{status}=await Permissions.askAsync(Permissions.CAMERA)
        this.setState({hasCameraPermission: status==='granted',})
    }
    async componentDidMount(){
        var user=firebase.auth().currentUser;
        await this.askPermission()
        await this.registerForPushNotificationsAsync(user)
    }
    handleSettings=()=>{
        this.props.navigation.navigate('SettingsScreen')
    }
    showDialog=(isShow)=>{
        this.setState({isDialogVisible:isShow})
    }      
    handleAccount=()=>{
        this.props.navigation.navigate('MyAccountScreen')
    }
    handleBarCodeRead = data => {
        this.showDialog(true);
        data=JSON.stringify(data);
        this.setState({promptVisible:true});
    }
    registerForPushNotificationsAsync=async(user)=>{
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
          );
          let finalStatus = existingStatus;
        
          // only ask if permissions have not already been determined, because
          // iOS won't necessarily prompt the user a second time.
          if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
          }
        
          // Stop here if the user did not grant permissions
          if (finalStatus !== 'granted') {
            return;
          }
          
          // Get the token that uniquely identifies this device
          let token = await Notifications.getExpoPushTokenAsync();
          var updates={};
          updates['/expoToken']=token;
          firebase.database().ref(user.uid).update(updates);

    }
    render(){
        const hasCameraPermission=this.state
        return(   
        <Container>    
        <Header style={{backgroundColor:'#08a529'}}>
        <Left>
        <Icon name='person' onPress={this.handleAccount} style={{color:'white'}} />
        </Left>
        <Body>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HomeScreen')}}>
              <Image style={{width:65, height:65, flex:0, alignItems:'center'}} source={require('../img/logo.png')}/>
              </TouchableOpacity>
        </Body> 
        <Right>
        <Icon name='settings' onPress={this.handleSettings} style={{color:'white'}}/>
        </Right>       
        </Header>
        <Body style={styles.container}>
        <Text>Point the camera over customer QR code...</Text>
        <View style={{marginVertical:20}}>
        {hasCameraPermission===null?Alert.alert('Camera permission null'):
         (hasCameraPermission===false?Alert.alert('Enable Camera Permission in Settings'):
        <Camera 
         onBarCodeRead={this.handleBarCodeRead}
         style={{height:200, width:200}}/>)}
         </View>
        <Button style={styles.clickButton}
                full
                rounded
                onPress={() => this.props.navigation.navigate('HomeScreen')}
               >
                         <Text style={{fontWeight: 'bold', color:'white'}}>Switch to User</Text>
        </Button>
        <Prompt
          title="Enter the purchased amount"
          defaultValue="0"
          visible={this.state.promptVisible}
          onCancel={()=>this.setState({promptVisible:false, message:'You cancelled'})}
          onSubmit={(value)=>this.setState({promptVisible:false, message:'Value received'})}/>
       <Text style={{fontSize:20}}>{this.state.message}</Text>
        </Body>
        </Container>
    )
    }}
    
    
const styles = StyleSheet.create({
    container: {
      alignItems:'center',  
      justifyContent:'center',
    },
    clickButton:{
        paddingVertical:15,
        marginVertical:15,
        backgroundColor:'#08a529',
        width:250
      },
    header:{
        backgroundColor:'#08a529'
    }, 
    failureText: { color: 'black', fontSize: 18, alignItems:'center', justifyContent:'center' } 
})



