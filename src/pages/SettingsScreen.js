import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  AsyncStorage,
  TouchableOpacity,
  Alert
} from "react-native";
import firebase from 'firebase';

import { Icon, Button, Container, Header,
Left, Body, Right } from 'native-base';




class SettingsScreen extends Component {
  constructor(props){

    super(props);
    this.state = {
      loaded: false,
    }
  
  }
  
  handleLogout(){
    AsyncStorage.removeItem('user_data').then(()=>{
      firebase.auth().signOut();
      this.props.navigation.navigate('Form')
    })
  }
  handleAccount=()=>{
    this.props.navigation.navigate('MyAccountScreen')
  }
  handleSettings=()=>{
    this.props.navigation.navigate('SettingsScreen')
  }
  handleChange=()=>{
    this.props.navigation.navigate('ChangePasswordScreen')
  }
  componentWillMount(){
  
    AsyncStorage.getItem('user_data').then((user_data_json) => {
      let user_data = JSON.parse(user_data_json);
      this.setState({
        user: user_data,
        loaded: true
      });
    });
  
  }
  render() {
    return (
      <Container>
      <Header style={styles.header}>
        <Left>
        <Icon name='person' onPress={this.handleAccount} style={{color:'white'}} />
        </Left>
        <Body>
          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('HomeScreen')}}>
          <Image style={{width:65, height:65, flex:0, alignItems:'center'}} source={require('../img/logo.png')}/>
          </TouchableOpacity>
        </Body>
        <Right>
        <Icon name='settings' onPress={this.handleSettings} style={{color:'white'}}/>
        </Right>  
        </Header>
      <View style={styles.container}>
      <Body style={styles.bodyStyle}>
      <Text>Welcome to Settings</Text>
      <Button style={styles.clickButton}
                full
                rounded
                
                onPress={() => this.handleChange()}
               >
                         <Text style={{fontWeight: 'bold', color:'white'}}>Reset Password</Text>
        </Button>
      <Button style={styles.clickButton}
                full
                rounded
                
                onPress={() => this.handleLogout()}
               >
                         <Text style={{fontWeight: 'bold', color:'white'}}>Logout</Text>
        </Button>
      </Body>
      </View>
      </Container>
    )
  }
}
export default SettingsScreen;
const styles=StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  header:{
    backgroundColor:'#08a529'
  },
  bodyStyle:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  clickButton:{
    paddingVertical:15,
    marginVertical:15,
    backgroundColor:'#08a529',
    width:250,
  },
})