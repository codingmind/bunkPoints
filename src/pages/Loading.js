import React,{Component} from 'react'
import  firebase from 'firebase'
import * as Animatable from 'react-native-animatable'
import { View, Text, ActivityIndicator, StyleSheet,StatusBar, Image, } from 'react-native';
export default class Loading extends Component {


  
componentDidMount(){
        firebase.auth().onAuthStateChanged(user=>
              { this.props.navigation.navigate(user?'HomeScreen':'Form') })}
  render() {
    return (
      <View style={styles.container}>
      <StatusBar
                backgroundColor='#08a529'
                barStyle="dark-content"
     />
    <Image style={{ width:100 , height:100}}
                 source={require('../img/logo.png')}/>
        <Animatable.Text animation="tada" iterationCount='infinite'>Shop...Earn...Redeem...</Animatable.Text>
       
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding:10,
  },
logoText: {
  marginVertical: 15,
  fontSize: 22,
  color:'white',
  justifyContent:'flex-end',
  alignItems:'center'
},})