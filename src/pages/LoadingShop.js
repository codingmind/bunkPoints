import React from 'react'
import  firebase from 'firebase'
import * as Animatable from 'react-native-animatable'
import { View, Text, ActivityIndicator, StyleSheet,StatusBar, Image, Alert } from 'react-native'
export default class Loading extends React.Component {



  
componentDidMount(){
    var user=firebase.auth().currentUser;
    firebase.database().ref(user.uid).on('value',(snapshot)=>{
        this.props.navigation.navigate(snapshot.val()?'ShopScreen':'LogintoShopScreen')
    })
}
  render() {
    return (
      <View style={styles.container}>
      <StatusBar 
                backgroundColor='#08a529'
                barStyle="dark-content"
     />
    <Image style={{ width:100 , height:100}}
                 source={require('../img/logo.png')}/>
        <Animatable.Text animation="tada" iterationCount='infinite'>Shop...Earn...Redeem...</Animatable.Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    padding:10,
  },
logoText: {
  marginVertical: 15,
  fontSize: 22,
  color:'white',
  justifyContent:'flex-end',
  alignItems:'center'
},})