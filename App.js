﻿import React,{Component} from 'react'
import {StyleSheet, Platform, Image, Text, View} from 'react-native'
import {createSwitchNavigator,createDrawerNavigator,DrawerItems} from 'react-navigation'
import { Container, Content, Header, Body, Icon } from 'native-base'

import Loading from './src/pages/Loading'
import Form from './src/pages/Form'
import HomeScreen from './src/pages/HomeScreen'
import SettingsScreen from './src/pages/SettingsScreen'
import MyAccountScreen from "./src/pages/MyAccountScreen";
import ShopScreen from './src/pages/ShopScreen';
import LoadingShop from './src/pages/LoadingShop';
import LogintoShopScreen from './src/pages/LogintoShopScreen';
import ForgotPasswordScreen from './src/pages/ChangePasswordScreen';
import ChangePasswordScreen from './src/pages/ChangePasswordScreen';
const App=createSwitchNavigator(
    {Loading,
    Form,
    HomeScreen,
    LoadingShop,
    LogintoShopScreen,
    ShopScreen,
    SettingsScreen,
    MyAccountScreen,
    ForgotPasswordScreen,
    ChangePasswordScreen
},
{
    initialRouteName:'Loading',
    headerMode:'none'
}
)

  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    drawerImage:{
        height:150,
        width:150,
        borderRadius:75
      }
})
export default App